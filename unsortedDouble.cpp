/******************************************************************************
* Name: Keeble, Erica
* ID: 7431673
* Section: M001
* Homework 14
*****************************************************************************/
#include "unsortedDouble.h"
#include <iostream>
#include <string>
#include <functional>

using namespace std;

unsortedDouble::unsortedDouble(unsortedDouble &l) // copy constructor
{
    node *src, *dest;

    //create empty list
    headptr = nullptr;
    tailptr = nullptr;

    //return if list we are trying to copy is empty
    if (l.headptr == nullptr)
        return;

    //assign source and destination
    src = l.headptr;
    dest = headptr;

    //create first node and copy in item
    dest = new (nothrow) node;
    headptr = dest;
    dest->item = src->item;
    dest->next = nullptr;
    tailptr = dest;
    dest->last = tailptr;
    src = src->next;

    //copy in all the rest of the items
    while (src != nullptr)
    {
        dest->next = new (nothrow) node;
        dest->next->last = dest;
        dest = dest->next;
        dest->item = src->item;
        dest->next = nullptr;
        tailptr = dest;
        src = src->next;
    }
    return;
}

//returns position of the item moving forwards in the list 
int unsortedDouble::retrieve(string val)
{
    //walk through doubly linked list until first val, then return val
    node * temp;
    temp = headptr;
    int index = 1;

    //if an empty list, return error right off the bat
    if (temp == nullptr)
        return 0;

    while (temp->item != val)
    {
        //go to next node, and increment index
        temp = temp->next;
        index++;

        //if you've reached the end of the list, return 0
        if (temp == nullptr)
        {
            return 0;
        }
    }

    return index;
}

//returns position of the item moving backwards in the list
int unsortedDouble::rretrieve(string val)
{
    //walk backwards through list until we reach val
    node * temp;
    temp = tailptr;
    int index = size();

    if (temp == nullptr)
        return 0;

    while (temp->item != val)
    {
        temp = temp->last;
        index--;

        if (temp == nullptr)
            return 0;
    }

    return index;
}

//will return the item at the position requested. returns true or false if it did
bool unsortedDouble::retrieve(string &item, int position)
{
    //walk through until position
    //but make sure position is within the size of the list, and greater than 1
    int lSize = size();
    if (position > lSize || position < 1)
        return false;

    int index = 1;
    node * temp;
    temp = headptr;
    while (index != position)
    {
        temp = temp->next;
        index++;
    }

    item = temp->item;
    return true;

}

//will return the number of items that match the requested data in the function
int unsortedDouble::countIf(function<bool(string)> cond)
{
    node * temp;
    temp = headptr;
    int count = 0;
    while (temp != nullptr)
    {
        if (cond(temp->item) == true)
            count++;
        temp = temp->next;
    }

    return count;
}

//Query if this object is empty. Both pointers must be nullptr.
bool unsortedDouble::isEmpty()
{
    if (headptr == nullptr && tailptr == nullptr)
        return true;
    else
        return false;
}

//Gets the number of nodes in the list. 
int unsortedDouble::size()
{
    //check if it's empty
    if (headptr == nullptr && tailptr == nullptr)
        return 0;

    //check if there is one node
    if (headptr == tailptr)
        return 1;

    //if there are many nodes, find how many
    int count = 0;
    node *temp;
    temp = headptr;
    while (temp != nullptr)
    {
        ++count;
        temp = temp->next;
    }
    return count;
}

//Pushes the given value to the end of the list. 
bool unsortedDouble::push_back(string val)
{
    //insert at end of list
    //I could have just used the insert function
    //I realize this after I got all the test cases working
    //I feel dumb now

    node *temp;
    temp = new (nothrow) node;
    if (temp == nullptr)
    {
        cout << "Not enough storage." << endl;
        return false;
    }
    temp->item = val;
    temp->next = nullptr;
    temp->last = nullptr;

    //if list was empty
    if (isEmpty())
    {
        headptr = temp;
        tailptr = temp;
        return true;
    }

    //if list already has stuff in it
    tailptr->next = temp;
    temp->next = nullptr;
    temp->last = tailptr;
    tailptr = temp;
    return true;
}

//Pushes a value to the front of the list. 
bool unsortedDouble::push_front(string val)
{
    //insert at front of list
    //I could have just used the insert function
    //I realize this after I got all the test cases working
    //I feel dumb now


    node *temp;
    temp = new (nothrow) node;
    if (temp == nullptr)
    {
        cout << "Not enough storage." << endl;
        return false;
    }
    temp->item = val;
    temp->next = nullptr;
    temp->last = nullptr;

    //if list was empty
    if (isEmpty())
    {
        headptr = temp;
        tailptr = temp;
        return true;
    }

    //if there was stuff in the list
    if (headptr != nullptr)
    {
        temp->next = headptr;
        headptr->last = temp;
        headptr = temp;
        return true;
    }

    return false;
}

/*Removes the value at the end of the list.
This value is copied into the variable val if it exists. */
bool unsortedDouble::pop_back(string &val)
{
    //remove at end of list
    //I could have just used the remove function
    //I realize this after I got all the test cases working
    //I feel dumb now

    node *temp;
    temp = tailptr;

    //if list is empty
    if (headptr == nullptr)
    {
        return false;
    }

    //if youre removing the last item in the list
    if (headptr == tailptr)
    {
        val = temp->item;
        headptr = nullptr;
        tailptr = nullptr;
        delete temp;
        return true;
    }

    //if theres stuff in the list
    if (headptr != nullptr)
    {
        val = temp->item;
        temp->last->next = nullptr;
        tailptr = temp->last;
        delete temp;
        return true;
    }

    return false;
}

/*Removes the value at the front of the list.
This value is copied into the variable val if it exists */
bool unsortedDouble::pop_front(string &val)
{
    //remove at front of list
    //I could have just used the remove function
    //I realize this after I got all the test cases working
    //I feel dumb now

    node *temp;
    temp = headptr;

    //if list is empty
    if (headptr == nullptr)
    {
        return false;
    }

    //if only one value in list
    if (headptr == tailptr)
    {
        val = temp->item;
        headptr = nullptr;
        tailptr = nullptr;
        delete temp;
        return true;
    }

    //if many values in the list
    if (headptr != nullptr)
    {
        val = temp->item;
        headptr = temp->next;
        temp->next->last = nullptr;
        delete temp;
        return true;
    }

    return false;
}

// exchange the contents of two lists
void unsortedDouble::swap(unsortedDouble &l)
{
    node *temp;

    //change the list given by headptr and tailptr with list l
    temp = l.headptr;
    l.headptr = headptr;
    headptr = temp;

    temp = l.tailptr;
    l.tailptr = tailptr;
    tailptr = temp;
}

//Removes the node at the give position in the list. The first node is at position 1. 
bool unsortedDouble::remove(int pos)
{
    node *curr;
    int count = 1;
    curr = headptr;

    //if invalid position
    if (pos <= 0)
    {
        return false;
    }

    //if list is empty
    if (headptr == nullptr)
    {
        return false;
    }

    //if removing last node from list
    if (curr->next == nullptr)
    {
        headptr = nullptr;
        tailptr = nullptr;
        return true;
    }

    //if removing first node
    if (pos == 1)
    {
        headptr = curr->next;
        curr->next->last = nullptr;
        delete curr;
        return true;
    }

    //traversal
    while (count < pos && curr->next != nullptr)
    {
        curr = curr->next;
        ++count;
    }
    //if invalid node
    if (count < pos)
    {
        return false;
    }
    //if removing from end
    if (curr->next == nullptr)
    {
        curr->last->next = nullptr;
        tailptr = curr->last;
        delete curr;
        return true;
    }
    //if in middle of list
    if (count == pos)
    {
        curr->last->next = curr->next;
        curr->next->last = curr->last;
        delete curr;
        return true;
    }
    return false;
}

//Removes the first given val that is found going forward in the list.
bool unsortedDouble::remove(string val)
{
    node *curr;
    curr = headptr;
    bool found = false;

    //traverse through list until item is found
    while (curr != nullptr && found == false)
    {
        //if item is found
        if (val.compare(curr->item) == 0)
        {
            found = true;
        }
        else
        {
            curr = curr->next;
        }
    }

    //if value is not found in list
    if (found == false)
    {
        return false;
    }

    //if value is at begining of list
    if (headptr == curr)
    {
        //if value is only value in list
        if (curr->next == nullptr)
        {
            headptr = nullptr;
            tailptr = nullptr;
            return true;
        }
        headptr = curr->next;
        curr->next->last = nullptr;
        delete curr;
        return true;
    }
    //if value is at end of list
    else if (curr->next == nullptr)
    {
        curr->last->next = nullptr;
        tailptr = curr->last;
        delete curr;
        return true;
    }
    //if value is in the middle of list
    else
    {

        curr->last->next = curr->next;
        curr->next->last = curr->last;
        delete curr;
        return true;
    }

    return false;
}

/*Searches for the first match for the given string starting from the front
of the list. */
bool unsortedDouble::find(string val)
{
    node *curr = headptr;
    bool found = false;

    //traverse through list until item is found
    while (curr != nullptr && found == false)
    {
        //if item is found
        if (val.compare(curr->item) == 0)
        {
            found = true;
            return true;
        }
        else
        {
            curr = curr->next;
        }
    }

    //if item was not found, return false
    if (found == false)
    {
        return false;
    }

    return false;
}

//Starts at the end of the list and searchs for the given value going backwards.
bool unsortedDouble::rfind(string val)
{
    node *curr = tailptr;
    bool found = false;

    //traverse through list until item is found
    while (curr != nullptr && found == false)
    {
        //if item is found
        if (val.compare(curr->item) == 0)
        {
            found = true;
            return true;
        }
        else
        {
            curr = curr->last;
        }
    }

    //if item was not found, return false
    if (found == false)
    {
        return false;
    }

    return false;
}

/*Default constructor will initialize an emtpy list with both pointers set
to nullptr. */
unsortedDouble::unsortedDouble()
{
    headptr = nullptr;
    tailptr = nullptr;
}

/*Clears this object to its blank / initial state. Both pointers will be set
to nullptr; */
void unsortedDouble::clear()
{
    node *curr;

    curr = headptr;

    //traversal
    while (curr != tailptr)
    {
        node *temp;
        temp = curr;
        curr = curr->next;
        delete temp;
    }

    headptr = nullptr;
    tailptr = nullptr;
}

//Destructor - will free all the nodes in the list. 
unsortedDouble::~unsortedDouble()
{
    clear();
}

/*Prints the list out either forwards or backwards using a seperator that was
passed in. Forward is the default using a seperator of ", ". Both of these
values can be changed. forward = false will print the list out backwards.
Any string can be used to seperate the lists values. NO new line will be
outputted on any output. */
void unsortedDouble::print(ostream &out, bool forward,
    string seperator)
{
    //print forwards
    if (forward == true)
    {
        node *curr;
        curr = headptr;
        while (curr != nullptr)
        {
            out << curr->item;
            if (curr->next != nullptr)
            {
                out << seperator;
            }
            curr = curr->next;
        }
    }
    //print backwards
    else
    {
        node *curr;
        curr = tailptr;
        while (curr != nullptr)
        {
            out << curr->item;
            if (curr->last != nullptr)
            {
                out << seperator;
            }
            curr = curr->last;
        }
    }
}


//inserts into doubly linked list
bool unsortedDouble::insert(string val, int pos)
{
    //define variables and allocate memory for temp
    int index = 0;
    node *temp;
    temp = new (nothrow) node;
    if (temp == nullptr)
    {
        cout << "Not enough memory." << endl;
        return false;
    }
    temp->item = val;
    temp->next = nullptr;
    temp->last = nullptr;

    //if invalid position, return false
    if (pos <= 0)
    {
        return false;
    }

    //insert into empty list
    if (isEmpty())
    {
        if (pos != 1)
        {
            return false;
        }
        headptr = temp;
        tailptr = temp;
        return true;
    }

    //insert into beg of list
    if (pos == 1)
    {
        temp->next = headptr;
        headptr->last = temp;
        headptr = temp;
        return true;
    }

    //insert into middle of list
    node *prev, *curr;
    prev = headptr;
    curr = headptr;
    while (curr != nullptr)
    {
        if (pos - 1 == index)
        {
            temp->next = curr;
            temp->last = prev;
            prev->next = temp;
            curr->last = temp;
            return true;
        }
        else
        {
            prev = curr;
            curr = curr->next;
            ++index;
        }

    }
    //insert at the end of list
    if (pos - 1 == index)
    {
        temp->next = nullptr;
        temp->last = prev;
        prev->next = temp;
        tailptr = temp;
        return true;
    }

    return false;
}
